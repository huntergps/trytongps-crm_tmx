# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from .party import Party, CustomersContext, WizardAddOpportunity, Employee,Address
from .opportunity import Opportunity

def register():
    Pool.register(
        Party,
        CustomersContext,
        Opportunity,
        Employee,
        Address,
        module='crm_tmx', type_='model')
    Pool.register(
        WizardAddOpportunity,
        module='crm_tmx', type_='wizard')
