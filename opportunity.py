    # This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import with_statement
from datetime import date
from trytond.model import ModelView, ModelSQL, fields, Workflow
from trytond.transaction import Transaction
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, In
#from trytond.modules.sale_opportunity import SaleOpportunityReportMixin

__all__ = ['Opportunity']

SUBJECTS = [
    ('phone', 'Phone Call'),
    ('schedule', 'Schedule'),
    ('rfq', 'R. Opportunity'),
    ('follow', 'Follow U'),
    ('volume', 'Lin-Volume L'),
    ('other', 'Other'),
    ('email', 'Email'),
]

_STATES_STOP = {
    'readonly': In(Eval('state'), ['converted', 'won', 'lost', 'cancelled']),
}

_DEPENDS_STOP = ['state']


class Opportunity:
    __metaclass__ = PoolMeta
    __name__ = 'sale.opportunity'
    ext_id = fields.Char('External Id', readonly=True,
        required=False, select=True)
    company_rel = fields.Many2One('party.party', 'Company Related')
    salesman = fields.Many2One('company.employee', 'Salesman', required=True,
            states=_STATES_STOP, depends=['state', 'company'],
        domain=[('company', '=', Eval('company'))])
    mail = fields.Char('Mail')
    phone = fields.Char('Phone')
    current_date = fields.Date('Current Date', required=False,
        select=True)
    subject_current_date = fields.Selection(SUBJECTS, 'Subject Current Date',
        select=True)
    next_date = fields.Date('Next Date', select=True)
    subject_next_date = fields.Selection(SUBJECTS, 'Subject Next Date',
        select=True)
    email_sended = fields.Many2One('electronic.mail', 'E-Mail Sended')
    interactions = fields.One2Many('sale.opportunity_interaction',
        'opportunity', 'Interactions')

    @classmethod
    def __setup__(cls):
        super(Opportunity, cls).__setup__()
        cls._buttons.update({
            'send_mail': {
                'invisible': Eval('email_sended'),
            },
        })

    @staticmethod
    def default_party():
        return Transaction().context.get('active_id')

    @staticmethod
    def default_description():
        return "Opportunity ..."     
    

    @fields.depends('party', 'phone', 'email', 'address', 'company_rel')
    def on_change_party(self):
        if not self.party:
            return
        if self.party.addresses:
            self.address = self.party.addresses[0].id
        if self.party.salesman:
            self.salesman = self.party.salesman
        self.phone = self.party.phone
        self.mail = self.party.email
        self.email_from = self.party.email
        if self.party.company:
            self.company_rel = self.party.company.id

    @staticmethod
    def default_employee():
        employee_id = Transaction().context.get('employee')
        if employee_id:
            return employee_id

    '''
    @staticmethod
    def default_salesman():
        employee_id = Transaction().context.get('employee')
        if employee_id:
            return employee_id
    '''
    @staticmethod
    def default_subject_next_date():
        return 'other'
        
    @staticmethod
    def default_subject_current_date():
        return 'phone'

    @classmethod
    @ModelView.button
    def send_mail(cls, opportunities):
        for opp in opportunities:
            cls._send_opportunity_mail(opp)

    @classmethod
    def _send_opportunity_mail(cls, opp):
        pool = Pool()
        MailTemplate = pool.get('electronic.mail.template')
        Interaction = pool.get('sale.opportunity_interaction')
        templates = MailTemplate.search([
            ('model.model', '=', 'sale.opportunity'),
        ])
        if not templates:
            return

        for template in templates:
            context = Transaction().context.copy()
            with Transaction().set_context(context):
                res = template.render_and_send([opp])
                if res:
                    cls.write([opp], {'email_sended': res.id})
                    Interaction.create([
                        {'opportunity': opp.id, 'subject': 'email'}
                    ])

