# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date
from trytond.model import ModelView, ModelSQL, fields, Workflow
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, In
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.transaction import Transaction
from datetime import date
from dateutil.relativedelta import relativedelta
from lxml import etree

__all__ = ['Party', 'CustomersContext','WizardAddOpportunity', 'Employee','Address']

SUBJECTS = [
    ('phone', 'Phone Call'),
    ('schedule', 'Schedule'),
    ('rfq', 'R. Opportunity'),
    ('follow', 'Follow U'),
    ('volume', 'Lin-Volume L'),
    ('other', 'Other'),
]

def change_labels(tags_xml,labels,view_type='form'):
    doc = etree.XML(tags_xml)
    if view_type == 'form':
        for campo,label in labels.items():
            node_campo="//label[@name='%s']"%campo
            for node in doc.xpath(node_campo):
                node.set('string',label)
    elif view_type == 'tree':
        for campo,label in labels.items():
            node_campo = "//field[@name='%s']"%campo
            for node in doc.xpath(node_campo):
                node.set('string',label)
    return doc   

class Employee:
    __metaclass__ = PoolMeta
    __name__ = 'company.employee'
    total_interactions = fields.Function(fields.Integer('Interactions'),
        'get_interactions')

    def get_interactions(self, name):
        Interactions = Pool().get('sale.opportunity_interaction')
        ctx = Transaction().context
        dom_inter = [
            ('opportunity.salesman', '=', self.id)
        ]
        if ctx.get('start_date'):
            dom_inter.append(('effective_date', '>=', ctx.get('start_date')))
        if ctx.get('end_date'):
            dom_inter.append(('effective_date', '<=', ctx.get('end_date')))

        interactions = Interactions.search(dom_inter)
        return len(interactions)


class Address:
    __metaclass__ = PoolMeta
    __name__ = 'party.address'
    '''   
    @classmethod
    def write(cls, *args):
        actions = iter(args)
        for addresses, values in zip(actions, actions):
            if 'party' in values:
                for address in addresses:
                    if address.party.id != values['party']:
                        cls.raise_user_error(
                            'write_party', (address.rec_name,))
        super(Address, cls).write(*args)
    '''
    
class Party:
    __metaclass__ = PoolMeta
    __name__ = 'party.party'
    ext_id = fields.Char('External Id', readonly=True,
        select=True)
    company = fields.Many2One('party.party', 'Company Related')
    sales_count = fields.Integer('Sales Count')
    salesman = fields.Many2One('company.employee', 'Salesman')
    is_salesman = fields.Boolean('Is Salesman')
    opportunities = fields.One2Many('sale.opportunity', 'party',
        'Opportunities', domain=[('party', '=', Eval('id'))])
    subdivision = fields.Function(fields.Many2One('country.subdivision',
        'State'), 'get_subdivision', searcher='search_subdivision')
    zip = fields.Function(fields.Char('Zip'), 'get_zip')
    city = fields.Function(fields.Char('City'), 'get_city')


    total_sales_count = fields.Function(fields.Integer('Total Sales Count'),'get_sales_count')
    total_quote = fields.Function(fields.Integer('Total Quotes'), 'get_total_quotes')
    total_job = fields.Function(fields.Integer('Total Jobs'), 'get_total_jobs')
    next_date = fields.Function(fields.Date('Next Date'),'get_next_date')
    subject_next_date = fields.Function(fields.Char('Subject Next Date'),'get_next_date')
    quote1 = fields.Integer('Quote1', select=True)
    job1 = fields.Integer('Job1',states={ 
            'invisible': Eval('context', {}).get('show1', False) 
            }, select=True)

    quote2 = fields.Integer('Quote2', select=True)
    job2 = fields.Integer('Job2', select=True)
    quote3 = fields.Integer('Quote3', select=True)
    job3 = fields.Integer('Job3', select=True)
    job4 = fields.Integer('Job4', select=True)
    job5 = fields.Integer('Job5', select=True)
    job6 = fields.Integer('Job6', select=True)
    job7 = fields.Integer('Job7', select=True)
    job8 = fields.Integer('Job8', select=True)
    job9 = fields.Integer('Job9', select=True)
    job10 = fields.Integer('Job10', select=True)
    job11 = fields.Integer('Job11', select=True)
    job12 = fields.Integer('Job12', select=True)

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()
        cls._buttons.update({
            'add_opportunity': {
                'invisible': False,
            },
        })





    @staticmethod
    def default_quote1():
        return 0

    @staticmethod
    def default_job1():
        return 0

    @classmethod
    def fields_view_get(cls, view_id=None, view_type='form'):
        '''
        Cambia las etiquetas de las columnas de trabajo, acorde al mes actual
        '''
        labels = {}
        for i in range(0,12):
            mydate =date.today() - relativedelta(months=i)
            if view_type=='form':
                labels['job%d'%(i+1)]= "Jobs %s"%mydate.strftime("%B %Y")
                if i<3:
                    labels['quote%d'%(i+1)]= "Quotes %s"%mydate.strftime("%B %Y")
            elif view_type=='tree':
                labels['job%d'%(i+1)]= mydate.strftime("%b \n%y")
                if i<3:
                    labels['quote%d'%(i+1)]= "Q%s"%mydate.strftime("%b \n%y")            
        res = super(Party, cls).fields_view_get(view_id, view_type)
        doc = change_labels(res['arch'],labels,view_type)
        res['arch'] = etree.tostring(doc)
        return res


    @classmethod
    def search(cls, args, offset=0, limit=None, order=None, count=False, query=False):
        args = args[:]

        ctx = Transaction().context
        try:
            if ctx['salesman']:
                args.append(('salesman', '=', ctx['salesman']))
        except:
            pass
        try:
            if ctx['subdivision']:
                id_sub = int(ctx['subdivision'])
                Subdivision = Pool().get('country.subdivision')
                subdivision, = Subdivision.search([('id', '=', id_sub)])                
                args.append(('subdivision', '=', subdivision.name))
        except:
            pass

        try:
            dom=[]
            if ctx['rango_months'] == 'current':
                dom =  [('job1', '>', 0)]

            if ctx['rango_months'] == '2_last':
                dom = [
                    ['OR', 
                        [('job1', '>', 0)],[('job2', '>', 0)]
                        ]
                    ]
            if ctx['rango_months'] == '3_last':
                dom = [
                    ['OR', 
                        [('job1', '>', 0)],[('job2', '>', 0)],[('job3', '>', 0)]
                        ]
                    ]            
            if ctx['rango_months'] == '4_last':
                dom = [
                    ['OR', 
                        [('job1', '>', 0)],[('job2', '>', 0)],[('job3', '>', 0)],
                        [('job4', '>', 0)]
                        ]
                    ]
            if ctx['rango_months'] == '5_last':
                dom = [
                    ['OR', 
                        [('job1', '>', 0)],[('job2', '>', 0)],[('job3', '>', 0)],
                        [('job4', '>', 0)],[('job5', '>', 0)]
                        ]
                    ]
            if ctx['rango_months'] == '6_last':
                dom = [
                    ['OR', 
                        [('job1', '>', 0)],[('job2', '>', 0)],[('job3', '>', 0)],
                        [('job4', '>', 0)],[('job5', '>', 0)],[('job6', '>', 0)]
                        ]
                    ]
            if ctx['rango_months'] == '7_last':
                dom = [
                    ['OR', 
                        [('job1', '>', 0)],[('job2', '>', 0)],[('job3', '>', 0)],
                        [('job4', '>', 0)],[('job5', '>', 0)],[('job6', '>', 0)],
                        [('job4', '>', 0)]
                        ]
                    ]                                                
            if ctx['rango_months'] == '8_last':
                dom = [
                    ['OR', 
                        [('job1', '>', 0)],[('job2', '>', 0)],[('job3', '>', 0)],
                        [('job4', '>', 0)],[('job5', '>', 0)],[('job6', '>', 0)],
                        [('job7', '>', 0)],[('job8', '>', 0)]
                        ]
                    ]  
            if ctx['rango_months'] == '9_last':
                dom = [
                    ['OR', 
                        [('job1', '>', 0)],[('job2', '>', 0)],[('job3', '>', 0)],
                        [('job4', '>', 0)],[('job5', '>', 0)],[('job6', '>', 0)],
                        [('job7', '>', 0)],[('job8', '>', 0)],[('job9', '>', 0)]
                        ]
                    ]  
            if ctx['rango_months'] == '10_last':
                dom = [
                    ['OR', 
                        [('job1', '>', 0)],[('job2', '>', 0)],[('job3', '>', 0)],
                        [('job4', '>', 0)],[('job5', '>', 0)],[('job6', '>', 0)],
                        [('job7', '>', 0)],[('job8', '>', 0)],[('job9', '>', 0)],
                        [('job10', '>', 0)]
                        ]
                    ]  
            if ctx['rango_months'] == '11_last':
                dom = [
                    ['OR', 
                        [('job1', '>', 0)],[('job2', '>', 0)],[('job3', '>', 0)],
                        [('job4', '>', 0)],[('job5', '>', 0)],[('job6', '>', 0)],
                        [('job7', '>', 0)],[('job8', '>', 0)],[('job9', '>', 0)],
                        [('job10', '>', 0)],[('job11', '>', 0)]
                        ]
                    ]
            if ctx['rango_months'] == '12_last':
                dom = [
                    ['OR', 
                        [('job1', '>', 0)],[('job2', '>', 0)],[('job3', '>', 0)],
                        [('job4', '>', 0)],[('job5', '>', 0)],[('job6', '>', 0)],
                        [('job7', '>', 0)],[('job8', '>', 0)],[('job9', '>', 0)],
                        [('job10', '>', 0)],[('job11', '>', 0)],[('job12', '>', 0)]
                        ]
                    ]                                                                                    
            args.append(dom)   
        except:
            pass
        return super(Party, cls).search(args, offset=offset, limit=limit, order=order, count=count, query=query)




    @classmethod
    @ModelView.button_action('crm_tmx.wizard_add_opportunity')
    def add_opportunity(cls, parties):
        pass

    def get_next_date(self, name):
        res = None
        Opportunity = Pool().get('sale.opportunity')
        opportunities = Opportunity.search([
                ('party', '=', self.id),
                ('next_date', '!=', None),
            ], order=[('next_date', 'DESC')])
        if opportunities:
            if 'subject' in name:
                res = dict(SUBJECTS)[opportunities[0].subject_next_date]
            else:
                res = opportunities[0].next_date
        return res

    def get_opportunity_month(self, name):
        res = None
        if 'current' in name:
            if 'quote' in name:
                res = getattr(self, 'quote1')
            elif 'job' in name:
                res = getattr(self, 'job1')
        elif 'last_two' in name:
            if 'quote' in name:
                res = getattr(self, 'quote2')
            elif 'job' in name:
                res = getattr(self, 'job2')
        elif 'last_three' in name:
            if 'quote' in name:
                res = getattr(self, 'quote3')
            elif 'job' in name:
                #res = (self.job1 or int('0'))+(self.job2 or int('0'))+(self.job3 or int('0'))
                res = getattr(self, 'job3')
        return res

    '''
    def get_opportunity_month(self, name):
        res = None
        today = date.today()
        month = today.month
        if 'current' in name:
            if 'quote' in name:
                res = getattr(self, 'quote' + str(month))
            elif 'job' in name:
                res = getattr(self, 'job' + str(month))
        elif 'last_two' in name:
            month = month - 1
            if month < 1:
                return
            if 'quote' in name:
                res = getattr(self, 'quote' + str(month))
            elif 'job' in name:
                res = getattr(self, 'job' + str(month))
        elif 'last_three' in name:
            month = month - 2
            if month < 1:
                return
            if 'quote' in name:
                res = getattr(self, 'quote' + str(month))
            elif 'job' in name:
                res = getattr(self, 'job' + str(month))
        return res
    '''

    def get_last_two_op(self, name):
        res = 0
        today = date.today()
        month = today.month
        if 'quote' in name:
            res = getattr(self, 'quote' + str(month))
        elif 'job' in name:
            res = getattr(self, 'job' + str(month))
        return res

    def get_total_opportunity(self, name):
        res = None
        """
        Historic = Pool().get('crm.historic_opportunity')
        ctx = Transaction().context
        today = date.today()
        month = today.month
        year = today.year
        start_month = None
        end_month = None
        dom = []
        if ctx['range_months'] == 'current':
            start_month = end_month = str(year) + '-' + str(month)
        elif ctx['range_months'] == 'two_last':
            start_month = str(year) + '-' + str(month - 1)
            end_month = str(year) + '-' + str(month)
        elif ctx['range_months'] == 'three_last':
            start_month = str(year) + '-' + str(month - 2)
            end_month = str(year) + '-' + str(month)

        if start_month:
            dom = [
                ('month', '>=', start_month),
                ('month', '<=', end_month),
            ]

        historics = Historic.search(dom)
        res = sum([getattr(h, name) for h in historics])
        """
        return res

    def get_total_jobs(self, name):
        res = 0
        res = (self.job1 or int('0'))+(self.job2 or int('0'))+(self.job3 or int('0'))+(self.job4 or int('0'))+(self.job5 or int('0'))+(self.job6 or int('0')) + (self.job7 or int('0'))+(self.job8 or int('0'))+(self.job9 or int('0'))+(self.job10 or int('0'))+(self.job11 or int('0'))+(self.job12 or int('0'))
        return res

    def get_total_quotes(self, name):   
        res = 0 
        res = (self.quote1 or int('0'))+(self.quote2 or int('0'))+(self.quote3 or int('0'))
        return res

    def get_sales_count(self, name):
        res = 0
        return res

    def get_subdivision(self, name):
        for address in self.addresses:
            if address.subdivision:
                return address.subdivision.id

    def get_zip(self, name):
        for address in self.addresses:
            if address.zip:
                return address.zip

    def get_city(self, name):
        for address in self.addresses:
            if address.city:
                return address.city

    @classmethod
    def search_subdivision(self, name, clause):     
        field_ = clause[0]
        field_name = 'addresses.' + field_ + '.name'
        return [(field_name, clause[1], clause[2])]

    def get_opportunities(self, name=None):
        pool = Pool()
        Opportunity = pool.get('sale.opportunity')
        opportunities = Opportunity.search([
            ('party', '=', self.id),
            ])
        opportunities_ids = [o.id for o in opportunities]
        return opportunities_ids


class CustomersContext(ModelView, Workflow, ModelSQL):
    'Customers Context'
    __name__ = 'crm.customers.context'
    rango_months = fields.Selection([
        ('current', 'Current Month'),
        ('2_last', '2 Last Months'),
        ('3_last', '3 Last Months'),
        ('4_last', '4 Last Months'),
        ('5_last', '5 Last Months'),  
        ('6_last', '6 Last Months'),  
        ('7_last', '7 Last Months'),  
        ('8_last', '8 Last Months'),  
        ('9_last', '9 Last Months'),  
        ('10_last', '10 Last Months'),  
        ('11_last', '11 Last Months'),  
        ('12_last', 'Last Year'),  
        ('all', 'All Customers'),        
    ], 'Range Months')
    company = fields.Many2One('company.company', 'Company', required=True)
    salesman = fields.Many2One('company.employee', 'Salesman')
    country = fields.Many2One('country.country', 'Country')
    subdivision = fields.Many2One("country.subdivision",
        'State', domain=[
            ('country', '=', Eval('country', -1)),
            ('parent', '=', None),
            ],depends=['active', 'country'])    
    subdivision2 = fields.Many2One('country.subdivision','State'),
    show1 = fields.Boolean('Show Job1')
    
    @classmethod
    def __setup__(cls):
        super(CustomersContext, cls).__setup__()
        cls._buttons.update({
            'current_month': {
                'invisible': False,
            },
            'two_last_month': {
                'invisible': False,
            },
            'three_last_month': {
                'invisible': False,
            },
        })


    @staticmethod
    def default_rango_months():
        return 'all'

    @staticmethod
    def default_country():
        return 61

    @staticmethod
    def default_show1():
        return False

class WizardAddOpportunity(Wizard):
    'Wizard Add Opportunity'
    __name__ = 'crm.add_opportunity'
    start = StateView('sale.opportunity',
        'crm_tmx.add_opportunity_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Add and New', 'add_new_', 'tryton-go-jump', default=True),
            Button('Add', 'add_', 'tryton-ok'),
        ])
    add_new_ = StateTransition()
    add_ = StateTransition()

    def default_start(self, fields):
        return {
            'party': Transaction().context.get('active_id'),
        }

    def add_opportunity(self):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        config = pool.get('sale.configuration')(1)
        self.start.party = Transaction().context.get('active_id', False)
        self.start.number = Sequence.get_id(
            config.sale_opportunity_sequence.id)
        self.start.save()

    def transition_add_new_(self):
        self.add_opportunity()
        return 'start'

    def transition_add_(self):
        self.add_opportunity()
        return 'end'


